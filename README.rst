Generazione del sito
====================

- Installare pelican (es. sudo apt install pelican)
  Versione testata: 4.0.1 su Debian 10. 

- Creare una cartella ed inserire all'interno di essa il contenuto di
  https://gitlab.com/glugcatania/catanialinux_website.git (configurazioni e gli rst) e
  https://gitlab.com/glugcatania/catanialinux_tema.git

- entrare nella cartella appena creata ed eseguire:

  ::
    
    make html
  
  e il sito generato sarà accessibile in una nuova cartella `output`.


Rigenerazione
~~~~~~~~~~~~~

Una volta inserito un nuovo articolo/pagina o modificato uno esistente,
per rigenerare i contenuti, così come per la generazione, basta eseguire:

::

  make clean
  make html

e il sito generato sarà accessibile in `output`.