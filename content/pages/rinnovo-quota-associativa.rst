Rinnovo Quota Associativa
#########################

.. raw:: html

    <!-- Image Header -->
    <img class="img-fluid rounded mb-4" src="images/content/rinnovo.png">

    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mb-4">
        <div id="smart-button-container">
          <div style="text-align: center;">
            <div style="margin-bottom: 1.25rem;">
              <h2>Rinnovo quota associativa GNU/Linux User Group Catania</h2>
              <ul class="text-center">
                <li>Per socio <strong>"Studente"</strong> la quota da pagare è pari a <strong>€ 5,00 ogni biennio.</strong></li>
                <li>Per socio <strong>"Ordinario"</strong> la quota da pagare è pari a <strong>€ 10,00 ogni anno.</strong></li>
              </ul>
              <p class="text-center">Per qualsiasi altra informazione consultare lo statuto al seguente: <a href="https://www.catania.linux.it/pdf/associazione/statuto.pdf" target="_blank">link</a> oppure scrivere a <a href="mailto:info@catania.linux.it">info[at]catania.linux.it</a></p>
              <div class="mx-auto mt-4 form-select">
                <p>Seleziona la quota associativa</p>
                <select id="item-options">
                  <option value="Studente" price="5">Studente - 5 EUR</option>
                  <option value="Ordinario" price="10">Ordinario - 10 EUR</option>
                </select>
              </div>
              <select style="visibility: hidden" id="quantitySelect"></select>
            </div>
            <div class="pb-5" id="paypal-button-container"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
