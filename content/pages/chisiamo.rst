Chi siamo
#########

.. raw:: html

    <!-- Intro Content -->
    <div class="row">
      <div class="col-lg-6">
        <img class="img-fluid rounded mb-4" src="images/associazione/crew.png" title="Membri GNU/Linux User Group Catania" alt="Membri GNU/Linux User Group Catania">
      </div>
      <div class="col-lg-6">
        <h2>Chi siamo</h2>
        <p class="text-justify">Associazione che raggruppa appassionati, studenti e professionisti uniti dalla passione per il FLOSS (Free/Libre and Open Source Software).</p>
        <p class="text-justify">Il nostro obiettivo è favorire la libera circolazione delle idee, delle conoscenze, e della volontarietà nella diffusione delle stesse, con particolare riferimento al campo informatico e al software libero, tra cui i sistemi operativi aperti, come GNU/Linux, i software con sorgenti liberamente accessibili, studiabili, adattabili e perfezionabili da parte dell’utente, in particolare nel mondo della scuola, negli enti pubblici, nell’industria ed in ogni altro settore in cui esso sia applicabile.</p>
        <p class="text-justify">Aperti alla divulgazione delle novità, organizziamo eventi a tema e corsi di formazione di alto profilo scientifico.</p>
      </div>
    </div>
    <!-- /.row -->

    <!-- Foto Eventi -->
    <h2>Alcune foto dei nostri eventi</h2>

    <div class="row">
      <div class="col-lg-4 mb-4">
        <div class="card h-100 text-center">
          <img class="card-img-top" src="images/associazione/linuxday_2019_1.png" title="GNU/Linux Day 2019" alt="GNU/Linux Day 2019">
        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100 text-center">
          <img class="card-img-top" src="images/associazione/linuxday_2019_2.png" title="GNU/Linux Day 2019" alt="GNU/Linux Day 2019">
        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100 text-center">
          <img class="card-img-top" src="images/associazione/sid_cannizzaro_2020.png" title="Safer Internet Day ITI S. Cannizzaro" alt="Safer Internet Day ITI S. Cannizzaro">
        </div>
      </div>
    </div>
    <!-- /.row -->

    <!-- Collaborazioni -->
    <h2>Collaborazioni</h2>
    <div class="row">
      <div class="col-lg-2 col-sm-4 mb-4 d-flex">
        <a href="https://www.unict.it" target="_blank"><img class="img-fluid" src="images/associazione/logo_unict.png" title="Università di Catania" alt="Università di Catania"></a>
      </div>
      <div class="col-lg-2 col-sm-4 mb-4 d-flex">
        <a href="http://web.dmi.unict.it" target="_blank"><img class="img-fluid" src="images/associazione/logo_dmi.png" title="Dipartimento di Matematica e Informatica" alt="Dipartimento di Matematica e Informatica"></a>
      </div>
      <div class="col-lg-2 col-sm-4 mb-4 d-flex">
        <a href="https://www.dieei.unict.it" target="_blank"><img class="img-fluid" src="images/associazione/logo_dieei.png" title="Dipartimento di Ingegneria Elettrica Elettronica e Informatica" alt="Dipartimento di Ingegneria Elettrica Elettronica e Informatica"></a>
      </div>
      <div class="col-lg-2 col-sm-4 mb-4 d-flex">
        <a href="https://www.itarchimede.it" target="_blank"><img class="img-fluid" src="images/associazione/logo_archimede.png" title="Istituto Tecnico Industriale Archimede Catania" alt="Istituto Tecnico Industriale Archimede Catania"></a>
      </div>
      <div class="col-lg-2 col-sm-4 mb-4 d-flex">
        <a href="http://cannizzaroct.edu.it" target="_blank"><img class="img-fluid" src="images/associazione/logo_cannizzaro.png" title="Istituto Tecnico Industriale S. Cannizzaro Catania" alt="Istituto Tecnico Industriale S. Cannizzaro Catania"></a>
      </div>
      <div class="col-lg-2 col-sm-4 mb-4 d-flex">
        <a href="https://www.icsdonmilanimisterbianco.edu.it" target="_blank"><img class="img-fluid" src="images/associazione/logo_milani.png" title="Istituto Comprensivo Statale Don Lorenzo Milani Misterbianco" alt="Istituto Comprensivo Statale Don Lorenzo Milani Misterbianco"></a>
      </div>
    </div>
    <!-- /.row -->
