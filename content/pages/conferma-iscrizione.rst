Conferma Iscrizione
###################

.. raw:: html

    <!-- Image Header -->
    <img class="img-fluid rounded mb-4" src="images/content/thank-you.png">

    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mt-2">
        <div id="success"></div>
        <!-- For success/fail messages -->
      </div>
    </div>
    <!-- /.row -->
