Mailing-Lists
#############

.. raw:: html

    <!-- Image Header -->
    <img class="img-fluid rounded mb-4" src="images/content/mailing-lists.png">

    <div class="container">
      <div class="row text-justify">
        <div class="col-lg-12 mx-auto">
          <h2 class="text-center">Come iscriversi alle varie Mailing Lists GLUGCT</h2>
          <hr>
          <p>Per iscriverti ad una Mailing-List bisogna prima <a href="https://lists.catania.linux.it/accounts/signup/?next=/hyperkitty" target="_blank">registrarsi quì</a></p>
          <p>Una volta inserite le tue credenziali, riceverai una email di conferma. Vai al link suggerito per completare la procedura di registrazione.</p>
          <p>Con le credenziali attive, accedi per iscriverti alle liste di tuo interesse.</p>
          <hr>
          <br>
          <h2 class="text-center">Linee Guida all'uso della Mailing List GLUGCT</h2>
          <hr>
          <p>Benvenuto nella Mailing List del GNU/Linux User Group Catania.</p>
          <p>All'interno delle liste (Linux, Lug, Projects) potrai discutere di tutti i temi relativi al mondo del free software, open source e partecipare all’organizzazione dei nostri eventi.</p>
          <hr>
          <p>Sei invitato ad aprire nuove discussioni e a partecipare a quelle attive.</p>
          <div><p>Ti chiediamo di rispettare le seguenti regole quando scrivi in queste liste:</p></div>
          <ul>
            <li>Scrivi paragrafi e messaggi corti ed essenziali, senza esagerare. Le Mailing Lists non sono chat.</li>
            <li>Ricordati di scrivere la tua risposta DOPO il testo citato (bottom quote), per agevolare la lettura dei messaggi in ordine cronologico.</li>
            <li>Quando citi un’altra persona, togli tutto ciò che non è correlato alla tua risposta.</li>
            <li>Focalizza un argomento per messaggio e includi sempre un testo pertinente nel campo oggetto (subject) del messaggio, in modo che gli altri utenti possano localizzarlo velocemente.</li>
            <li>Avvisa il direttivo (<a href="mailto:admin@catania.linux.it">admin[at]catania.linux.it</a>) prima di inviare qualunque offerta di lavoro in lista.</li>
            <li>Il limite massimo degli allegati è < di 1 MB</li>
          </ul>
          <div><p>Altri suggerimenti utili:</p></div>
          <ul>
            <li>Utilizza lettere maiuscole solo per sottolineare un punto importante o per distinguere un titolo o un sottotitolo dal resto del testo. Scrivere in maiuscole intere parole che non sono titoli viene generalmente definito URLARE!</li>
            <li>Asterischi o underscore intorno ad una parola posso aiutare a evidenziare e sottolineare un termine.</li>
            <li>Identifica tutte le citazioni, i riferimenti e le fonti delle informazioni che vuoi divulgare e rispetta i copyright e gli eventuali accordi per la divulgazione di qualsiasi informazione.</li>
            <li>E’ considerato estrema maleducazione l’inoltrare comunicazioni personali e/o private a Mailing List senza l’esplicita autorizzazione dell’autore.</li>
            <li>Fai attenzione al sarcasmo e all’umorismo. Senza la comunicazione facciale e l’intonazione della voce, la tua battuta di spirito potrebbe essere intesa come una critica. Quando vuoi esprimere qualche particolare inflessione, usa le emoticons. Per esempio, :) = sorriso (implica una battuta).</li>
          </ul>
          <div><p>Per leggere la guida completa, fai <a href="pdf/content/come_quotare_bene.pdf" target="_blank">click quì</a></p></div>
          <hr>
          <br>
          <h2 class="text-center">Utilizzi un account GMail? Leggi sotto</h2>
          <hr>
          <p>Se scrivi sulla Mailing-List utilizzando un account GMail, Google sa che hai già una copia nella cartella <strong>Posta Inviata</strong> e getta via la tua copia proveniente dalla Mailing-List. Ma per l'uso specifico con le Mailing-List è probabile che la copia della Mailing-List sia più utile della copia della cartella <strong>Posta Inviata</strong>. Sfortunatamente, questa politica è consolidata e Google si rifiuta esplicitamente di cambiarla. Nonostante diverse segnalazioni inviate al supporto di GMail.</p>
          <p>Questa anomalia, non è attribuibile agli amministratori di questa Mailing-List, ma bensì ad una precisa scelta voluta da Google.</p>
          <p>La seguente procedura permette di ricevere una copia dei propri messaggi scritti, trovando un compromesso per "ingannare" l'insana scelta di Google.</p>
          <ul>
          <li>Vai su GMail ed accedi con il tuo account.</li>
          <li>Vai su <strong>impostazioni</strong> (icona ingranaggio in alto a destra).</li>
          <li>Fai click sul pulsante <strong>Visualizza tutte le impostazioni</strong>.</li>
          <li>Dal menu in alto, fai click su <strong>Filtri e indirizzi bloccati</strong>.</li>
          <li>Fai click su <strong>Crea nuovo filtro</strong>.</li>
          <li>Si apre una finestra e scrivi nel campo <strong>Da</strong> il tuo indirizzo email (tuoindirizzo@gmail.com).</li>
          <li>Lasciare tutti gli altri campi vuoti e fai click su <strong>Crea filtro</strong>.</li>
          <li>Si apre una finestra con diversi "settaggi". Spunta solo <strong>Non inviare mai a Spam</strong>.</li>
          <li>Vai avanti creando il filtro facendo click su <strong>Crea filtro</strong>.</li>
          <li>Esci da GMail.</li>
          </ul>
        </div>
      </div>
    </div>
