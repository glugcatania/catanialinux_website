Conferma Donazione
##################

.. raw:: html

    <!-- Image Header -->
    <img class="img-fluid rounded mb-4" src="images/content/thank-you.png">

    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12">
        <div>
          <p class="text-justify">Ti ringraziamo per il contributo che hai voluto dare al nostro progetto, questo ci fa sentire che ci sei vicino e hai spostato la nostra causa.</p>
          <p class="text-justify">Se vuoi rimanere aggiornato sulle nostre attività iscriviti alle nostre Mailing-Lists <a href="mailing-lists.html" target="_self">cliccando quì</a></p>
        </div>
      </div>
    </div>
    <!-- /.row -->
