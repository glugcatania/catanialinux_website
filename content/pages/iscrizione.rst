Iscrizione
##########

.. raw:: html

    <!-- Image Header -->
    <img class="img-fluid rounded mb-4" src="images/content/iscrizione.png">

    <!-- Registration Form -->
    <div class="row">
      <div class="col-lg-12 mb-4 mx-auto">
        <h2 class="text-center mb-2">Iscrizione al GNU/Linux User Group Catania</h2>
        <p class="text-center">L'iscrizione all'associazione GLUGCT implica il pagamento di una quota:</p>
        <ul class="text-center">
          <li>Per socio <strong>"Studente"</strong> la quota è pari a <strong>€ 5,00 ogni biennio.</strong></li>
          <li>Per socio <strong>"Ordinario"</strong> la quota è pari a <strong>€ 10,00 ogni anno.</strong></li>
        </ul>
        <p class="text-center">Per qualsiasi altra informazione consultare lo statuto al seguente: <a href="https://www.catania.linux.it/pdf/associazione/statuto.pdf" target="_blank">link</a> oppure scrivere a <a href="mailto:info@catania.linux.it">info[at]catania.linux.it</a></p>
      </div>
    </div>

    <!-- Subscribe Form -->
    <div class="row">
      <div class="col-lg-8 mb-4 mx-auto">
        <h4 class="text-center mt-2">Compila il modulo di iscrizione</h4>
        <form name="sentSubscription" id="subscribeForm" novalidate>
         <div class="control-group form-group">
           <div class="controls">
             <label>Socio</label>
             <select class="form-control" id="socio">
               <option value="Studente">Studente</option>
               <option value="Ordinario">Ordinario</option>
             </select>
             <p class="help-block"></p>
           </div>
         </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Cognome</label>
              <input type="text" class="form-control" maxlength="20" id="cognome" required data-validation-required-message="Scrivi il tuo cognome.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Nome</label>
              <input type="text" class="form-control" maxlength="20" id="nome" required data-validation-required-message="Scrivi il tuo nome.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Data di nascita</label>
              <input class="form-control" type="date" id="data_nascita" required data-validation-required-message="Scrivi la tua data di nascita.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Codice fiscale</label>
              <input type="text" class="form-control" maxlength="16" id="codfisc" required data-validation-required-message="Scrivi il tuo codice fiscale.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Indirizzo</label>
              <input type="text" class="form-control" maxlength="150" id="indirizzo" required data-validation-required-message="Scrivi il tuo indirizzo.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>CAP</label>
              <input type="text" class="form-control" maxlength="5" id="cap" required data-validation-required-message="Scrivi il tuo CAP.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Provincia</label>
              <input type="text" class="form-control" maxlength="2" id="prov" required data-validation-required-message="Scrivi la tua Provincia.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Città</label>
              <input type="text" class="form-control" maxlength="50" id="citta" required data-validation-required-message="Scrivi la tua Città.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Email</label>
              <input type="email" class="form-control" maxlength="100" id="email" required data-validation-required-message="Scrivi il tuo indirizzo email.">
            </div>
          </div>
          <div class="control-group form-group mb-3">
            <div class="controls">
              <input type="checkbox" class="form-check-control" id="policy" required data-validation-required-message="Bisogna accettare la Privacy Policy.">
              <label class="ml-1">Accetta la <a href="privacy-policy.html" target="_blank">Privacy Policy</a></label>
            </div>
          </div>
          <div id="success"></div>
          <!-- For success/fail messages -->

          <button type="submit" class="btn btn-primary" id="sendSubscriptionButton"><i class="fa fa-pencil-alt"></i> Iscriviti</button>
        </form>
      </div>
    </div>
