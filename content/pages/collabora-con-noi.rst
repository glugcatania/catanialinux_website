Collabora con noi
#################

.. raw:: html

    <!-- Image Header -->
    <img class="img-fluid rounded mb-4" src="images/content/collabora-con-noi.png">

    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12">
        <div>
          <p class="text-justify">Il GNU/Linux User Group di Catania è sempre aperto a tutti coloro che vogliono condividere le proprie conoscenze liberamente.</p>
          <p class="text-justify">Se ritieni che la tua esperienza possa essere di aiuto a qualcuno, scrivi un articolo o un tutorial ed invialo all’indirizzo <a href="mailto:admin@catania.linux.it">admin[at]catania.linux.it</a>. A breve verrai contattato da un componente del direttivo per la pubblicazione sulla sezione <a href="documenti.html" target="_self">documenti</a> del nostro sito web.</p>
          <p class="text-justify">Unico requisito richiesto è che gli argomenti devono essere inerenti al mondo FLOSS (Free/Libre and Open Source Software).</p>
        </div>
      </div>
    </div>
    <!-- /.row -->
