Contatti
########

.. raw:: html

    <!-- Content Row -->
    <div class="row">
      <!-- Map Column -->
      <div class="col-lg-6 mb-4">
        <!-- Embedded OpenStreetMap -->
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=14.711380004882814%2C37.30300626322479%2C15.178298950195314%2C37.617494852086566&amp;layer=mapnik"></iframe>
      </div>
      <!-- Contact Details Column -->
      <div class="col-lg-6 mb-4">
        <h3>Mailing Lists</h3>
        <hr>
        <p>GNU/Linux -- Per supporto tecnico puoi usare questa lista</p>
        <p><abbr title="Mailing List"><i class="fas fa-envelope-square"></i></abbr> <a href="https://lists.catania.linux.it/hyperkitty/list/linux@catania.linux.it" target="_blank">linux[at]catania.linux.it</a></p>
        <hr>
        <p>La lista storica del GLUG Catania dedicata alla libera comunicazione</p>
        <p><abbr title="Mailing List"><i class="fas fa-envelope-square"></i></abbr> <a href="https://lists.catania.linux.it/accounts/login/?next=/accounts/login" target="_blank">lug[at]catania.linux.it</a></p>
        <hr>
        <p>Progetti del GLUG Catania</p>
        <p><abbr title="Mailing List"><i class="fas fa-envelope-square"></i></abbr> <a href="https://lists.catania.linux.it/accounts/login/?next=/accounts/login" target="_blank">projects[at]catania.linux.it</a></p>
        <hr>
        <!-- <p>Chat</p> -->
        <!-- <p><abbr title="Chat"><i class="fas fa-paper-plane"></i></abbr> address chat</p> -->
      </div>
    </div>
    <!-- /.row -->

    <!-- Contact Form -->
    <div class="row">
      <div class="col-lg-8 mb-4 mx-auto">
        <h4 class="text-center mb-2">Per comunicare con noi, compila il seguente form.</h4>
        <form name="sentMessage" id="contactForm" novalidate>
          <div class="control-group form-group">
            <div class="controls">
              <label>Cognome e Nome</label>
              <input type="text" class="form-control" id="name" required data-validation-required-message="Scrivi il tuo Cognome e Nome.">
              <p class="help-block"></p>
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Indirizzo Email</label>
              <input type="email" class="form-control" id="email" required data-validation-required-message="Scrivi il tuo indirizzo email.">
            </div>
          </div>
          <div class="control-group form-group">
            <div class="controls">
              <label>Messaggio</label>
              <textarea rows="5" cols="100" class="form-control" id="message" required data-validation-required-message="Scrivi il tuo messaggio" maxlength="999" style="resize:none"></textarea>
            </div>
          </div>
          <div class="control-group form-group mb-3">
            <div class="controls">
              <input type="checkbox" class="form-check-control" id="policy" required data-validation-required-message="Bisogna accettare la Privacy Policy.">
              <label class="ml-1">Accetta la <a href="privacy-policy.html" target="_blank">Privacy Policy</a></label>
            </div>
          </div>
          <div id="success"></div>
          <!-- For success/fail messages -->

          <button type="submit" class="btn btn-primary" id="sendMessageButton"><i class="fa fa-envelope"></i> Invia Messaggio</button>
        </form>
      </div>

    </div>
    <!-- /.row -->
