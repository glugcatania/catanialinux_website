Pagamento Ordinario
###################

.. raw:: html

    <!-- Content Row -->
    <div class="row">
      <div class="col-lg-12 mb-4">
        <div id="smart-button-container">
          <div style="text-align: center;">
            <div style="margin-bottom: 1.25rem;">
              <h2>Iscrizione al GNU/Linux User Group Catania</h2>
              <p class="text-center">L'iscrizione all'associazione GLUGCT implica il pagamento di una quota:</p>
              <p class="text-center">Per qualsiasi altra informazione consultare lo statuto al seguente: <a href="https://www.catania.linux.it/pdf/associazione/statuto.pdf" target="_blank">link</a> oppure scrivere a <a href="mailto:info@catania.linux.it">info[at]catania.linux.it</a></p>
              <div id="smart-button-container">
                <div style="text-align: center;">
                  <div id="paypal-button-container"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
