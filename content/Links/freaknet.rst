FreakNet Medialab
=================

:title: FreakNet Medialab
:authors: blackout69
:slug: link_1
:date: 2020-04-21
:modified: 2020-04-21
:card_image: freaknet.png
:card_url: https://www.freaknet.org
:card_button_label: Sito web

Il FreakNet MediaLab è nato come primo laboratorio italiano autogestito di informatica libera. costruito quasi interamente con hardware regalato da persone di buona volontà e di pezzi di vecchi computer risalenti dalla metà degli anni settanta fino alla tecnologia odierna, letteralmente salvati da discariche e cassonetti.
