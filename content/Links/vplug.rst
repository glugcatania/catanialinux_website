VPLUG
=====

:title: Vallelunga Pratameno GLUG
:authors: blackout69
:slug: link_8
:date: 2020-04-21
:modified: 2020-04-21
:card_image: vplug.png
:card_url: https://www.vplug.it
:card_button_label: Sito web

Lo scopo dell'associasione è quello di migliorare i prodotti con Licenza aperta, sostenerne la progettazione, mettere a disposizione le proprie conoscenze per realizzare progetti coerenti con le nostre idee e diffondere e promuovere il Software Libero rilasciato con licenza GNU GPL, FDL o LGPL redatte dalla Free Software Foundation e Software con altre licenze libere.
