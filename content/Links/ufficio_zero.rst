Ufficio Zero Linux
==================

:title: Ufficio Zero Linux
:authors: blackout69
:slug: link_7
:date: 2021-03-03
:modified: 2021-03-03
:card_image: ufficio_zero.png
:card_url: https://www.ufficiozero.org/
:card_button_label: Sito web

Ufficio Zero Linux è un progetto tutto italiano, rinato nell'aprile del 2020, che si prefige lo scopo di offrire dei sistemi Out of the Box, quindi completi, ed è rivolto principalmente a liberi professionisti ma anche enti privati e statali che volessero adottare i nostri sistemi, che si basano su diverse versioni linux (Linux Mint, Devuan, PCLinuxOS e LMDE) ma che hanno tutte lo stesso parco software omogeneo.
