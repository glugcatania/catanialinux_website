LUG Siracusa
============

:title: LUG Siracusa
:authors: danterolle
:slug: link_4
:date: 2020-04-21
:modified: 2020-04-21
:card_image: lugsiracusa.png
:card_url: http://siracusa.linux.it
:card_button_label: Sito web

Il Linux User Group di Siracusa nasce come riferimento per gli utenti di Software Libero ed in particolare di GNU/Linux nella provincia di Siracusa. Nasce e agisce in maniera libera e autonoma, ed è indipendente da qualsiasi finalità o condizionamento di tipo economico o politico. Promuove in ambito locale iniziative che favoriscano la diffusione del sistema operativo GNU/Linux e del software libero.
