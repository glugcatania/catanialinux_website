SoLiRa
======

:title: SoLiRa
:authors: danterolle
:slug: link_6
:date: 2020-04-21
:modified: 2020-04-21
:card_image: solira.png
:card_url: http://www.solira.org
:card_button_label: Sito web

L’associazione Software Libero Ragusa (SoLiRa) si è costituita nell’Ottobre del 2003, per formalizzare gli impegni che i soci già da tempo portavano avanti come Ragusa Linux User Group.
L'associazione si occupa di diffondere la cultura del Software Libero sul territorio di Ragusa attraverso l'organizzazione di conferenze e seminari a tema.
