Il Nostro GNU/Linux Day 2014
============================

:title: Il Nostro GNU/Linux Day 2014
:authors: Emanuele
:slug: evento_linuxday2014
:date: 2014-11-12
:modified: 2014-11-12 11:26:41 +0100
:locandina: nld_20141121_locandina.pdf
:download: 
:card_image: nld_20141121_banner.png

Il Nostro GNU/Linux Day 2014, giornata dedicata alla divulgazione della Cultura Libera del Libero Software e delle Libere Applicazioni dei Sistemi Operativi basati su Kernel Linux... e non solo!
