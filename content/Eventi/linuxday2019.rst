Catania GNU/Linux Day 2019
==========================

:title: Catania GNU/Linux Day 2019
:authors: danterolle
:slug: evento_linuxday2019
:date: 2019-10-25
:modified: 2019-10-25 12:30:00
:locandina: ld2019_locandina.pdf
:download: 
:card_image: ld2019_banner.png

Non ci fermiamo dal 2016! 
Anche quest'anno si terrà a Catania il GNU/Linux Day 2019!
il Linux Day è un'iniziativa distribuita per conoscere ed approfondire Linux ed il software libero. Si compone di numerosi eventi locali, organizzati autonomamente da gruppi di appassionati nelle rispettive città, tutti nello stesso giorno. In tale contesto puoi trovare talks, workshops, spazi per l’assistenza tecnica, gadgets, dibattiti e dimostrazioni pratiche. 
Dove e quando? 
Sabato 26 ottobre dalle ore 09:00 alle 19:00 presso l'aula magna di Ingegneria (Ed. 14) - Cittadella Universitaria.
Il tema di quest'anno riguarda l'Artificial Intelligence, Machine Learning, Big Data... Ma non mancheranno talks anche sulle vulnerabilità Google, Database noSQL, IPTV, pacchetti Debian e altro! 
L'accesso al Linux Day è libero e gratuito!