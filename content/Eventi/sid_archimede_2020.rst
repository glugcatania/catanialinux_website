Safer Internet Day I.T. Archimede
=================================

:title: Safer Internet Day I.T. Archimede
:authors: danterolle
:slug: evento_sid_archimede_2020
:date: 2020-02-15
:modified: 2020-02-03
:locandina: locandina_sid_archimede_2020.pdf
:download: slide_sid_archimede_2020.pdf
:card_image: sid_archimede_2020.png

Il Safer Internet Day (SID) è la giornata mondiale per la sicurezza in Rete istituita e promossa dalla Commissione Europea per un uso consapevole della rete, un ruolo attivo e responsabile di ciascuno per rendere internet un luogo positivo e sicuro.
