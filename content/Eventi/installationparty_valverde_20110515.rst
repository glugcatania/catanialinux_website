Il 04/06/2011 GNU/Linux Installation Party a Valverde
=====================================================

:title: Il 04/06/2011 GNU/Linux Installation Party a Valverde
:authors: Emanuele
:slug: evento_installationparty_valverde_20110515
:date: 2011-05-15
:modified: 2011-05-15 13:18:56 +0200
:locandina: installationparty_valverde_20110515.pdf
:download: 
:card_image: installationparty_valverde_20110515_banner.png

I soci del *GNU/Linux User Group di Catania* saranno lieti di incontrare
neo utenti GNU/Linux, simpatizzanti o semplici curiosi che vogliano
provare o abbiano bisogno di consigli sull’installazione di Sistemi
Operativi liberi o sul Software Libero in genere.

Vi aspettiamo numerosi dalle **ore 15:30** (ora Italiana) del **4 Giugno
2011** in **Via Eremo S. Anna 41 a Valverde (CT)**.

**Percorso**: Dalla piazza principale di Valverde andare fino in fondo
al Corso V.Emanuele e svoltare a sinistra per via del Santuario. Seguire
le indicazioni stradali per Eremo S.Anna.