Safer Internet Day ITI S. Cannizzaro
====================================

:title: Safer Internet Day ITI S. Cannizzaro
:authors: blackout69
:slug: evento_sid_cannizzaro_2020
:date: 2020-03-02
:modified: 2020-02-28
:locandina: locandina_sid_cannizzaro_2020.pdf
:download: slide_sid_cannizzaro_2020.pdf
:card_image: sid_cannizzaro_2020.png

Il Safer Internet Day (SID) è la giornata mondiale per la sicurezza in Rete istituita e promossa dalla Commissione Europea per un uso consapevole della rete, un ruolo attivo e responsabile di ciascuno per rendere internet un luogo positivo e sicuro.
