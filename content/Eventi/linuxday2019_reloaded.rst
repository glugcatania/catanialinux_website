Catania GNU/Linux Day 2019 Reloaded
===================================

:title: Catania GNU/Linux Day 2019 Reloaded
:authors: danterolle
:slug: evento_linuxday2019_reloaded
:date: 2019-11-22
:modified: 2019-11-22 12:30:00
:locandina: ld2019r_locandina.pdf
:download:
:card_image: ld2019r_banner.png

Dato l'impegno profuso nell'organizzazione dell'evento e la sua, sfortunata, sospensione a causa dell'allerta meteo del 26 ottobre, abbiamo comunque deciso di riproporre l'edizione catanese del GNU/Linux Day 2019 ribattezzandolo "Reloaded". Il Linux Day è un'iniziativa distribuita per conoscere ed approfondire Linux ed il software libero. Si compone di numerosi eventi locali, organizzati autonomamente da gruppi di appassionati nelle rispettive città, tutti nello stesso giorno. In tale contesto puoi trovare talks, workshops, spazi per l’assistenza tecnica, gadgets, dibattiti e dimostrazioni pratiche.

Dove e quando?

Sabato 23 novembre, seguiranno degli interventi dalle 09:00 alle 13:00, e delle attività dalle 14:30 alle 19:00 presso il DIEEI (Ed. 14) - Cittadella Universitaria. Il tema di quest'anno riguarda l'Artificial Intelligence, Machine Learning, Big Data... Ma non mancheranno talks anche sulle vulnerabilità Google, Database noSQL, IPTV, pacchetti Debian e altro!
L'accesso al GNU/Linux Day Reloaded è libero e gratuito!
