Richard Stallman a Catania il 25 Marzo
======================================

:title: Richard Stallman a Catania il 25 Marzo
:authors: Administrator
:slug: evento_richard_stallman_a_catania_il_25_marzo
:date: 2006-03-06
:modified: 2006-03-06 00:00:00 +0100
:locandina: rms_ct_2006.pdf
:download: 
:card_image: rms_ct_2006_banner.png

Sabato 25 Marzo 2006 Richard Stallman sarà a Catania per un seminario
sul progetto GNU e sul pericolo dei brevetti software. L’evento,
organizzato da LUGCT – GNU/Linux User Group Catania in collaborazione
con Freaknet, storico hacklab siciliano, si terra’ presso l’Aula Magna
della Facolta’ di Ingegneria dell’Universita’ di Catania. Stallman,
iniziatore del progetto GNU e padre fondatore della filosofia del
Software Libero, terra’ un seminario sulla storia del progetto GNU e
dell’apporto della comunita’ GNU allo sviluppo del sistema operativo
GNU/Linux. Nel pomeriggio e’ previsto un seminario sui pericoli
connessi alla brevettabilita’ del software e degli algoritmi,
argomento molto sentito dalla comunita’ del Software Libero, preceduto
da un breve intervento a cura di LUGCT e Freaknet sullo stato attuale
dei “diritti digitali in Italia”.