Etica e nuvole: la sfida dei valori nel cloud computing
=======================================================

:title: Etica e nuvole: la sfida dei valori nel cloud computing
:authors: danterolle
:slug: news_eticanuvole
:date: 2017-05-04
:modified: 2017-05-04 12:30:00

Il 5 maggio 2017 alle ore 9:30 si terrà a Catania, presso l'Auditorium Giancarlo de Carlo (Monastero Benedettini), l'evento Etica e nuvole: la sfida dei valori nel cloud computing.
Ingresso gratuito, non mancate!
Ospite speciale: Richard M. Stallman, presidente della Free Software Foundation. 
https://www.fsf.org/events/rms-20170505-catania