Iscrizione al GLUG CT
=====================

:title: [!] Iscrizione al GLUG CT
:authors: blackout69
:slug: form_iscrizione_soci_07-05-2021
:date: 2021-05-07
:modified: 2021-05-07

Il GLUG Catania è lieto di annunciare la possibilità di iscriversi all'associazione tramite l'apposita compilazione del form di iscrizione.

Per iscriverti, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="iscrizione.html" target="_self">clicca quì.</a>
