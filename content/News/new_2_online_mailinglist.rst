Mailing List nuovamente online!
===============================

:title: Mailing List nuovamente online!
:authors: blackout69
:slug: new_2
:date: 2020-08-23
:modified: 2020-08-23

L'associazione GNU/Linux User Group di Catania, annuncia la messa online della Mailing List. Gestione completamente rinnovata e totalmente tradotta in Italiano.

Per iscriverti, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="https://lists.catania.linux.it/accounts/login/?next=/postorius/lists" target="_blank">clicca quì.</a>
