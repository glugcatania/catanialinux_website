Collabora con noi
=================

:title: Collabora con noi
:authors: blackout69
:slug: new_7
:date: 2022-02-11
:modified: 2022-02-11

GNU/Linux User Group di Catania è sempre aperto a collaborare con quanti desiderano mettere a disposizione le proprie conoscenze nell'ambito del FLOSS (Free/Libre and Open Source Software).

Se sei interessato a collaborare con noi, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="collabora-con-noi.html" target="_self">clicca quì.</a>
