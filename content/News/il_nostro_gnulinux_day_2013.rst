Il Nostro GNU/Linux Day 2013
============================

:title: Il Nostro GNU/Linux Day
:modified: 2013-11-13 12:14:44 +0100
:date: 2013-11-13 
:slug: il_nostro_gnulinux_day_2013
:authors: Emanuele

Quest’anno il “GNU/Linux User Group Catania” organizza “Il Nostro
GNU/Linux Day”, giornata catanese dedicata alla divulgazione della
Cultura Libera e del Libero Software e delle Libere Applicazioni dei
Sistemi Operativi Liberi. Sistemi Operativi basati sul kernel Linux. Con
la partecipazione del Museo dell’Informatica Funzionante di Palazzolo
Acreide (SR).

| Potrete essere informati sulle novità  di: Linux, GNU, Open Source.
| Potrete ammirare la fedele ricostruzione con parti originali del primo
  computer Apple – APPLE I e perfettamente funzionante.
