Catania Linux Day 2016
======================

:title: Catania Linux Day 2016
:authors: danterolle
:slug: news_linuxday2016
:date: 2016-10-21
:modified: 2016-10-21 12:30:00

Il 22 ottobre si svolgerà a Catania il Linux Day e come in molte altre città d’Italia, si parlerà e ci si confronterà sullo stato dell’arte del mondo informatico e sugli inevitabili impatti che ha sulla vita
quotidiana di tutti noi. Quest’anno, in particolare ricorre il venticinquesimo anniversario della nascita di Linux e quindi lo SputniX di Catania organizza una serie di incontri presso la Cittadella Universitaria.
Libera manifestazione, libero ingresso.