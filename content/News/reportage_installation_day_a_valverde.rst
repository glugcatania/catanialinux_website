Reportage Installation Day a Valverde
=====================================

:title: Reportage Installation Day a Valverde
:modified: 2010-11-30 10:48:08 +0100
:date: 2010-11-30 
:slug: reportage_installation_day_a_valverde
:authors: pietro

Sabato 27 Novembre si è tenuta un pomeriggio di installazione di linux e
aiuto per gli utenti che si approcciano per la prima volta al mondo
dell’open source, anche l’aiuto a chi come me, magari usa software
libero da anni ma non ha mai programmato e si trovava a sbattere la
testolina sul tavolo per far funzionare quattro righe di codice che
aveva scritto. 

La giornata si è svolta con una riunione del GLug la mattina,  nella
bellissima sede messaci a disposizione da Mirthrandir e dalla sua
associazione, vicino all’eremo di S. Anna a  Valverde. Dopo una vivace
discussione, seguita anche dal buon Tosky che tra le spalate di neve dal
balcone ci seguiva ed interveniva attraverso la rete, siamo giunti
all’ora di pranzo. Qui le doti culinarie di Sarik si sono espresse in
una buona pasta con la salsa e pecorino per imbiancare il rosso della
salsa.

Nel pomeriggio ci siamo dedicati ai pc, aiutando chi ne avesse bisogno,
sia tecnicamente che mentalmente, cioè abbiamo dato qualche spiegazione
di base e qualche dritta su dove trovare tutte le info necessarie per
imparare da se stessi di più su linux e l’opensource.

Sabato 27 Novembre si è tenuta un pomeriggio di installazione di linux e
aiuto per gli utenti che si approcciano per la prima volta al mondo
dell’open source, anche l’aiuto a chi come me, magari usa software
libero da anni ma non ha mai programmato e si trovava a sbattere la
testolina sul tavolo per far funzionare quattro righe di codice che
aveva scritto. 

La giornata si è svolta con una riunione del GLug la mattina,  nella
bellissima sede messaci a disposizione da Mirthrandir e dalla sua
associazione, vicino all’eremo di S. Anna a  Valverde. Dopo una vivace
discussione, seguita anche dal buon Tosky che tra le spalate di neve dal
balcone ci seguiva ed interveniva attraverso la rete, siamo giunti
all’ora di pranzo. Qui le doti culinarie di Sarik si sono espresse in
una buona pasta con la salsa e pecorino per imbiancare il rosso della
salsa.

Nel pomeriggio ci siamo dedicati ai pc, aiutando chi ne avesse bisogno,
sia tecnicamente che mentalmente, cioè abbiamo dato qualche spiegazione
di base e qualche dritta su dove trovare tutte le info necessarie per
imparare da se stessi di più su linux e l’opensource.
