Convocazione Assemblea dei Soci
===============================

:title: Convocazione Assemblea dei Soci
:authors: blackout69
:slug: convocazione_assemblea_soci_01-03-2021
:date: 2021-03-01
:modified: 2021-03-01

Con la presente si comunica che è convocata l’assemblea ordinaria dei soci del GLUGCT. Si informano i signori soci iscritti che l’assemblea si terrà in presenza presso la sede del Codacons in Via Musumeci, 171 ed in videoconferenza al seguente link https://open.meet.garr.it/glugct2021 per permettere il regolare svolgimento, per il giorno 15 Marzo 2021, alle ore 06.30 ed ove occorra in seconda convocazione per il giorno 16 Marzo 2021 alle ore 18.00, per discutere e deliberare sul seguente...

Per leggerla, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="pdf/associazione/convocazione_assemblea_soci_01-03-2021.pdf" target="_blank">clicca quì.</a>
