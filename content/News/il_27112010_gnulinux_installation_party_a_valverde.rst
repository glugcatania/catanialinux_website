Il 27/11/2010, GNU/Linux Installation Party a Valverde
======================================================

:title: Il 27/11/2010, GNU/Linux Installation Party a Valverde
:modified: 2010-11-08 17:00:13 +0100
:date: 2010-11-08 
:slug: il_27112010_gnulinux_installation_party_a_valverde
:authors: Ivan Iraci

I soci del *GNU/Linux User Group di Catania* saranno lieti di incontrare
neo utenti GNU/Linux, simpatizzanti o semplici curiosi che vogliano
provare o abbiano bisogno di consigli sull’installazione di Sistemi
Operativi liberi o sul Software Libero in genere.

Vi aspettiamo numerosi dalle **ore 15:30** (ora Italiana) del **27
Novembre 2010** in **Via Eremo S. Anna 41 a Valverde (CT)**.

**Percorso**: Dalla piazza principale di Valverde andare fino in fondo
al Corso V.Emanuele e svoltare a sinistra per via del Santuario. Seguire
le indicazioni stradali per Eremo S.Anna. 

