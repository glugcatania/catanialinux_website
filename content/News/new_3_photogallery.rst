Photogallery online
===================

:title: [!] Nuova Photogallery online!
:authors: blackout69
:slug: new_3
:date: 2020-10-15
:modified: 2020-10-15

GNU/Linux User Group di Catania annuncia la pubblicazione della nuova sezione Photogallery. Una raccolta delle migliori foto storiche e non degli eventi organizzati dalla nostra associazione.

Per vederla, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="photogallery.html" target="_self">clicca quì.</a>
