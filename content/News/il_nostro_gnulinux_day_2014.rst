Il Nostro GNU/Linux Day 2014
============================

:title: Il Nostro GNU/Linux Day 2014
:modified: 2014-11-12 11:26:41 +0100
:date: 2014-11-12
:slug: il_nostro_gnulinux_day_2014
:authors: Emanuele

Il Nostro GNU/Linux Day 2014, giornata dedicata alla divulgazione della Cultura Libera del Libero Software e delle Libere Applicazioni dei Sistemi Operativi basati su Kernel Linux... e non solo!
