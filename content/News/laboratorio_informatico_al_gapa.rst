Laboratorio informatico al GAPA
===============================

:title: Laboratorio informatico al GAPA
:modified: 2009-10-20
:date: 2009-10-20 14:14:06 +0200
:slug: laboratorio_informatico_al_gapa
:authors: Emanuele

È in funzione al `centro di quartiere
GAPA <https://www.associazionegapa.org/>`__ in via Cordai il laboratorio
informatico allestito a cura del Gnu/Linux User Group di Catania. Il
laboratorio comprende 6 computer che erano stati dismessi perché
ritenuti obsoleti dai precedenti proprietari e riportati alla piena
efficienza tramite piccoli interventi di manutenzione e installazione di
Ubuntu. Il laboratorio è utilizzato dal GAPA, con l’assistenza del GLUG
Catania,  per corsi gratuiti di informatica di base e office
applications.
