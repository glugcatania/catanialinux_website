Iscrizione al GLUG CT con pagamento quota associativa tramite paypal
====================================================================

:title: [!] Iscrizione al GLUG CT e pagamento quota associativa tramite paypal
:authors: blackout69
:slug: form_iscrizione_soci_paypal_11-07-2021
:date: 2021-07-11
:modified: 2021-07-11

Da oggi il GLUG Catania rende disponibile paypal come metodo di pagamento delle quote associative.  

Per iscriverti, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="iscrizione.html" target="_self">clicca quì.</a>
