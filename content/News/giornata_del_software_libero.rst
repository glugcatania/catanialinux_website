Giornata del Software Libero
============================

:title: Giornata del Software Libero
:modified: 2009-11-22 13:21:11 +0100
:date: 2009-11-22 
:slug: giornata_del_software_libero
:authors: Ivan Iraci

| Il 28 novembre avrà luogo congiuntamente a Catania e a Ragusa la
  **“Giornata del Software Libero”**.

La manifestazione, consistente in una serie di brevi seminari, è
patrocinata a Catania dal Dipartimento di Matematica e Informatica e a Ragusa dal Comune di Ragusa.


| I seminari in programma avranno luogo in mattinata a Catania dalle ore
  09:30 nell’Aula Magna del Dipartimento di Matematica e Informatica
  alla Cittadella Universitaria di Viale Andrea Doria e nel pomeriggio a
  Ragusa dalle 16:30 nell’Aula Consiliare del Palazzo dell’Aquila in
  Corso Italia 72.


Le due sessioni saranno curate dai LUG locali (GLUG-Catania e
Solira-Ragusa) con la partecipazione del Freaknet Medialab.


