Nuovo Progetto con l'ICT Don Milani di Misterbianco
===================================================

:title: Nuovo Progetto con l'ICT Don Milani di Misterbianco
:authors: blackout69
:slug: new_5
:date: 2021-12-22
:modified: 2021-12-22

Il GLUG Catania sta portando avanti un nuovo progetto con l'ICS Don Milani di Misterbianco per l'installazione di GNU/Linux su circa 37 PC disposti in due aule di informatica da tempo non più utilizzate.
Chiunque volesse saperne di più e/o collaborare a questo nuovo progetto, potrà seguire gli sviluppi sulla Mailing-List lug[at]catania.linux.it
