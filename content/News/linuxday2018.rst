Catania GNU/Linux Day 2018
==========================

:title: Catania GNU/Linux Day 2018
:authors: danterolle
:slug: news_linuxday2018
:date: 2018-10-26
:modified: 2018-10-26 12:30:00

Sabato 27 ottobre tornerà a Catania la principale manifestazione italiana dedicata a GNU/Linux, al software libero, alla cultura aperta ed alla condivisione!
Dopo le due precedenti edizioni del GNU/Linux Day ed Etica Web, evento con ospite speciale il Dr. Stallman fondatore di GNU e della FSF, anche quest’anno l'associazione Sputnix di Catania organizza la diciottesima edizione del GNU/Linux Day presso l'aula magna messa gentilmente a disposizione dall'ITI S. Cannizzaro dedicando una serie di conferenze agli aspetti tecnici ed etici dei sistemi GNU/Linux, del Free Software e dell’Open Source.
Ingresso libero. 