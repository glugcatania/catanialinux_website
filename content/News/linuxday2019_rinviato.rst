IMPORTANTE - CATANIA GNU/LINUX DAY 2019
=======================================

:title: [!] Catania GNU/Linux Day 2019 rinviato!
:authors: danterolle
:slug: news_linuxday2019_rinviato
:date: 2019-10-26
:modified: 2019-10-26 07:00:00

Per motivi di sicurezza derivati dall'allerta meteo, siamo costretti ad annullare il Catania Linux Day 2019!
È ufficiale, c'abbiamo sperato fino all'ultimo ma ne siamo letteralmente impossibilitati. Sarà rimandato a data da destinarsi. Continuate a seguirci per aggiornamenti!
