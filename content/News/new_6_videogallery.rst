Videogallery online
===================

:title: [!] Nuova Videogallery online!
:authors: blackout69
:slug: new_6
:date: 2022-01-10
:modified: 2022-01-10

GNU/Linux User Group di Catania annuncia la pubblicazione della nuova sezione Videogallery. Una raccolta di video storici e non degli eventi organizzati dalla nostra associazione.

Per vederla, |subscribe_link|

.. |subscribe_link| raw:: html

  <a href="videogallery.html" target="_self">clicca quì.</a>
