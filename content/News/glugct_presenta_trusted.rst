GLUGCT presenta: Trusted??
==========================

:title: GLUGCT presenta: Trusted??
:modified: 2008-07-08
:date: 2008-07-08 12:00:00 +0200
:slug: glugct_presenta_trusted
:authors: Luigi Toscano

Ci siamo! Il piccolo video introduttivo su Trusted Computing e Software
Libero prodotto dal GNU/Linux User Group Catania in occasione del
LinuxDay 2007 è finalmente disponibile!

Video: |video_link|

.. |video_link| raw:: html

  <a href="https://diode.zone/videos/watch/41618728-70bf-4f23-9361-1e7c583e38b8" target="_blank">Play now!</a>


Licenza: |licenza_link|

.. |licenza_link| raw:: html

  <a href="https://creativecommons.org/licenses/by-sa/3.0/" target="_blank">CC-BY-SA</a>


Il video è stato realizzato interamente con software libero.

Potete anche ammirare(?) il |flickr_link|

.. |flickr_link| raw:: html

  <a href="https://www.flickr.com/photos/ivan_iraci/sets/72157602426234456/" target="_blank">Backstage</a>

(grazie alle foto di Ivan!)
