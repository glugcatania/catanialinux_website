Videogallery
############

:title: Videogallery
:authors: blackout69
:slug: videogallery_1
:date: 2022-01-10
:modified: 2022-01-10
:titolo_1: Trusted Italiano
:video_1: https://diode.zone/videos/embed/41618728-70bf-4f23-9361-1e7c583e38b8?warningTitle=0&amp;peertubeLink=0
:titolo_2: Trusted Inglese
:video_2: https://diode.zone/videos/embed/5b4e57f7-3425-4db5-8e15-65a13a7752f0?warningTitle=0&amp;peertubeLink=0
:titolo_3: GNU/Linux Day Catania 2014 1 di 2
:video_3: https://diode.zone/videos/embed/f23fc471-60e9-4867-9f32-7e792f54f2d7?warningTitle=0&amp;peertubeLink=0
:titolo_4: GNU/Linux Day Catania 2014 2 di 2
:video_4: https://diode.zone/videos/embed/9e32b0f1-e2de-4835-abb4-4a7c35d5ef16?warningTitle=0&amp;peertubeLink=0
