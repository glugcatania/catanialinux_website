Clone HDD remoto
================

:title: Clone HDD remoto
:authors: calianto
:slug: documento_2
:date: 2020-10-14
:modified: 2020-10-14
:download: clone_hdd_remoto.pdf
:card_image: clone_hdd_remoto.png
:license:

Vuoi clonare un Hard Disk di un sistema GNU/Linux avviato, da remoto. Quale software usare?
