Sistema Operativo Ver. 1.0
==========================

:title: Sistema Operativo Ver. 1.0
:authors: calianto
:slug: documento_1
:date: 2017-08-27
:modified: 2017-08-27
:download: sistema_operativo_1.pdf
:card_image: sistema_operativo.png
:license: CC BY-SA 4.0

LINUX - Carrellata su: COMANDI USI & INSTALLAZIONE. Appunti su GNU/Linux ed esempi di installazione principalmente su Debian 8.6.
