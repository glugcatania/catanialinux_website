Introduzione ad Arch Linux
==========================

:title: Introduzione ad Arch Linux
:authors: eclisse
:slug: documento_4
:date: 2021-12-25
:modified: 2021-12-25
:download: intro_arch_linux.pdf
:card_image: intro_arch_linux.png
:license:

Introduzione ad Arch Linux. Guida di base su come installare la distro Arch Linux con XFCE.
