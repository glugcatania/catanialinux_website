#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import time

# General settings
USE_FOLDER_AS_CATEGORY = True
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
DELETE_OUTPUT_DIRECTORY = False
DISPLAY_BREADCRUMBS = True

OUTPUT_PATH = 'output'
PATH = 'content'
PAGE_PATHS = ['pages']
ARTICLE_PATHS = ['News', 'Eventi', 'Documenti', 'Links', 'Photogallery', 'Videogallery']
SITENAME = 'GLUG - Catania'
SITEURL = ''
STATIC_PATH = 'themes/images'
STATIC_PATHS = ['pdf', 'images']
#WITH_FUTURE_DATES = False
#SLUGIFY_SOURCE = 'basename'
#FORMATTED_FIELDS = ['summary', 'coveracknowledgement']

# URL settings
RELATIVE_URLS = True

ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}.html'

PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

CATEGORY_URL = '{slug}.html'
CATEGORY_SAVE_AS = '{slug}.html'

#TAG_URL = 'tag_{slug}.html'
#TAG_SAVE_AS = 'tag_{slug}.html'

#AUTHOR_URL = 'author_{slug}.html'
#AUTHOR_SAVE_AS = 'author_{slug}.html'

#ARCHIVES_SAVE_AS = 'archives.html'
#ARCHIVES_URL = 'archives.html'

#CATEGORIES_SAVE_AS = 'categories.html'
#CATEGORIES_URL = 'categories.html'

#TAGS_SAVE_AS = 'tags.html'
#TAGS_URL = 'tags.html'

# Content path
PATH_CONTENT_PDF = 'pdf/content'
PATH_CONTENT_IMG = 'images/content'
PATH_PHOTOGALLERY_IMG = 'images/photogallery'

# Time and Date
TIMEZONE = "Europe/Rome"

# Template pages
# TEMPLATE_PAGES = {}
DIRECT_TEMPLATES = ['index']
PAGINATED_TEMPLATES = {'category': None, 'news': None, 'eventi': None, 'documenti': None, 'links': None, 'photogallery': None, 'videogallery': None}

# Metadata
AUTHOR = 'GLUGCT'
FOOTER = time.strftime("%Y") + ' | GNU/Linux User Group - Catania'

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ATOM = None
FEED_RSS = None
FEED_ALL_ATOM = None
FEED_ALL_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
TAG_FEED_ATOM = None
TAG_FEED_RSS = None
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None

# Pagination
DEFAULT_PAGINATION = 6	# Scegliere un valore multiplo di 3 per via delle card mostrate 3 per row

# Translations
DEFAULT_LANG = 'it'
PATH_PHOTOGALLERY_IMG

# Themes
THEME = 'themes'
THEME_STATIC_DIR = 'themes'
THEME_STATIC_PATHS = ['static', 'mail', 'form']

MENU_ITEMS = (
	('Associazione', '#'),
	('News', 'news.html'),
	('Eventi', 'eventi.html'),
	('Documenti', 'documenti.html'),
	('Gallery', '#'),
	('Contatti', 'contatti.html'),
	('Links', 'links.html')
)

SUB_MENU_ASSOCIAZIONE = (
	('Chi siamo', 'chi-siamo.html'),
	('Statuto', 'pdf/associazione/statuto.pdf'),
	('Regolamento', 'pdf/associazione/regolamento.pdf'),
	('Mailing-Lists', 'mailing-lists.html'),
	('Collabora con noi', 'collabora-con-noi.html'),
	('Iscrizione', 'iscrizione.html'),
	('Rinnovo', 'rinnovo-quota-associativa.html'),
	('Webmail',	'https://mail.catania.linux.it')
)

SUB_MENU_GALLERY = (
	('Photogallery', 'photogallery.html'),
	('Videogallery', 'videogallery.html')
)

# Reading only modified content
LOAD_CONTENT_CACHE = False
CACHE_PATH = 'cache'
CHECK_MODIFIED_METHOD = 'mtime'
CACHE_CONTENT = False
CONTENT_CACHING_LAYER = 'reader'
GZIP_CACHE = True
WRITE_SELECTED = []
